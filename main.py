import os
import sys

from beeprint import pp

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, './BoardGame/'))
sys.path.append(BASE_PATH)

from BoardGame.Game import Chess
from BoardGame.Figure import Location

def start():
    game = Chess()
    while True:
        print("Введите движение в формате 'e2 e4'. Ходят: " + game.currentMoveColor + " Q - выход.")
        inputData = input()
        
        if inputData == "Q":
            print("Выход )")
            return
        try:
        	locations = makeLocations(inputData)
        	game.move(locations[0], locations[1])
        except Exception as e:
        	# pp(e)
        	print("Ошибка: " + str(e))
        

def makeLocations(stringLocation):
    return [
        Location(stringLocation[0], stringLocation[1]),
        Location(stringLocation[3], stringLocation[4])
    ]

if __name__ == '__main__':
    start()
    # game = Chess()
    # pp(game)
    # locations = makeLocations('e2 e4')
    # game.move(locations[0], locations[1])