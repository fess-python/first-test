import unittest
from unittest.mock import MagicMock
from beeprint import pp

import os
import sys

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../BoardGame'))
sys.path.append(BASE_PATH)

from Game import Chess

class ChessTestCase(unittest.TestCase):
    def test_init_figureSeted(self):
    	# Arrange

    	# Act
    	game = Chess();

    	# Assert
    	pp(game)


if __name__ == '__main__':
    unittest.main()
