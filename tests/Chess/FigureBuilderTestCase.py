import unittest
from unittest.mock import MagicMock
from beeprint import pp

import os
import sys

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../../BoardGame'))
sys.path.append(BASE_PATH)

from Chess.FigureBuilder import FigureBuilder
from Chess.FigureBuilder import FigureTypeENUM
from Figure import Figure
from MoveVector import MoveVector
from MoveVector import CellTypeEnum
from MoveVector import MoveTypeEnum

class FigureBuilderTestCase(unittest.TestCase):
    def getBuilderWithFakeGetVectors(self):
        # MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        figureBuilder = FigureBuilder()

        figureBuilder.getPawnVectors = MagicMock(return_value = [
            MoveVector(1, 0, 1)
        ])

        figureBuilder.getRookVectors = MagicMock(return_value = [
            MoveVector(1, 0, 2)
        ])

        figureBuilder.getKnightVectors = MagicMock(return_value = [
            MoveVector(1, 0, 3)
        ])

        figureBuilder.getBishopVectors = MagicMock(return_value = [
            MoveVector(1, 0, 4)
        ])

        figureBuilder.getQueenVectors = MagicMock(return_value = [
            MoveVector(1, 0, 5)
        ])

        figureBuilder.getKingVectors = MagicMock(return_value = [
            MoveVector(1, 0, 6)
        ])

        return figureBuilder;

    def test_getDiagonalMoveVecotrs(self):
        # Arrange
        figureBuilder = FigureBuilder()

        expected = [
            MoveVector(1, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, -1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, -1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ]

        # Act
        actual = figureBuilder.getDiagonalMoveVectors(3)

        # Assert
        self.assertEqual(expected, actual)

    def test_getVerticalMoveVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        expected = [
            MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, -1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ]

        # Act
        actual = figureBuilder.getVerticalMoveVectors(3)

        # Assert
        self.assertEqual(expected, actual)
    
    def test_getHorizontalMoveVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        expected = [
            MoveVector(1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ]

        # Act
        actual = figureBuilder.getHorizontalMoveVectors(3)

        # Assert
        self.assertEqual(expected, actual)

    def test_getPawnVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        expected = [
            MoveVector(0, 3, maxStep = 2, cellType = CellTypeEnum.TYPE_EMPTY, moveType = MoveTypeEnum.TYPE_FIRST),
            MoveVector(0, 3, maxStep = 1, cellType = CellTypeEnum.TYPE_EMPTY, moveType = MoveTypeEnum.TYPE_NOT_FIRST),
            MoveVector(1, 3, maxStep = 1, cellType = CellTypeEnum.TYPE_ENEMY_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 3, maxStep = 1, cellType = CellTypeEnum.TYPE_ENEMY_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getPawnVectors(3)

        # Assert
        self.assertEqual(expected, actual)

    def test_getRookVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()
        figureBuilder.getVerticalMoveVectors = MagicMock(return_value = [
            MoveVector(1, 0, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ])
        figureBuilder.getHorizontalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])

        expected = [
            MoveVector(-1, 0, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, 0, None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getRookVectors()

        # Assert
        self.assertEqual(expected, actual)
        figureBuilder.getVerticalMoveVectors.assert_called_once_with(None)
        figureBuilder.getHorizontalMoveVectors.assert_called_once_with(None)

    def test_getKnightVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        expected = [
            MoveVector(-1, 2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, 2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, -2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, -2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-2, -1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-2, 1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(2, -1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(2, 1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getKnightVectors()

        # Assert
        self.assertEqual(expected, actual)

    def test_getBishopVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        figureBuilder.getDiagonalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])

        expected = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getBishopVectors()

        # Assert
        self.assertEqual(expected, actual)
        figureBuilder.getDiagonalMoveVectors.assert_called_once_with(None)

    def test_getQueenVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        figureBuilder.getDiagonalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])
        figureBuilder.getVerticalMoveVectors = MagicMock(return_value = [
            MoveVector(1, 0, 7, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ])
        figureBuilder.getHorizontalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])

        expected = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, 0, 7, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getQueenVectors()

        # Assert
        self.assertEqual(expected, actual)
        figureBuilder.getDiagonalMoveVectors.assert_called_once_with(None)
        figureBuilder.getVerticalMoveVectors.assert_called_once_with(None)
        figureBuilder.getHorizontalMoveVectors.assert_called_once_with(None)

    def test_getKingVectors(self):
        # Arrange
        figureBuilder = FigureBuilder()

        figureBuilder.getDiagonalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])
        figureBuilder.getVerticalMoveVectors = MagicMock(return_value = [
            MoveVector(1, 0, 7, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY)
        ])
        figureBuilder.getHorizontalMoveVectors = MagicMock(return_value = [
            MoveVector(-1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])

        expected = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 0, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 3, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, 0, 7, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.getKingVectors()

        # Assert
        self.assertEqual(expected, actual)
        figureBuilder.getDiagonalMoveVectors.assert_called_once_with(1)
        figureBuilder.getVerticalMoveVectors.assert_called_once_with(1)
        figureBuilder.getHorizontalMoveVectors.assert_called_once_with(1)

    def test_getVectorsByFigureType_pawmType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_PAWN, 3)

        # Assert
        figureBuilder.getPawnVectors.assert_called_once_with(3)
        figureBuilder.getRookVectors.assert_not_called()
        figureBuilder.getKnightVectors.assert_not_called()
        figureBuilder.getBishopVectors.assert_not_called()
        figureBuilder.getQueenVectors.assert_not_called()
        figureBuilder.getKingVectors.assert_not_called()

    def test_getVectorsByFigureType_rookType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_ROOK)

        # Assert
        figureBuilder.getPawnVectors.assert_not_called()
        figureBuilder.getRookVectors.assert_called_once_with()
        figureBuilder.getKnightVectors.assert_not_called()
        figureBuilder.getBishopVectors.assert_not_called()
        figureBuilder.getQueenVectors.assert_not_called()
        figureBuilder.getKingVectors.assert_not_called()

    def test_getVectorsByFigureType_KnightType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_KNIGHT)

        # Assert
        figureBuilder.getPawnVectors.assert_not_called()
        figureBuilder.getRookVectors.assert_not_called()
        figureBuilder.getKnightVectors.assert_called_once_with()
        figureBuilder.getBishopVectors.assert_not_called()
        figureBuilder.getQueenVectors.assert_not_called()
        figureBuilder.getKingVectors.assert_not_called()

    def test_getVectorsByFigureType_BishopType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_BISHOP)

        # Assert
        figureBuilder.getPawnVectors.assert_not_called()
        figureBuilder.getRookVectors.assert_not_called()
        figureBuilder.getKnightVectors.assert_not_called()
        figureBuilder.getBishopVectors.assert_called_once_with()
        figureBuilder.getQueenVectors.assert_not_called()
        figureBuilder.getKingVectors.assert_not_called()

    def test_getVectorsByFigureType_QueenType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_QUEEN)

        # Assert
        figureBuilder.getPawnVectors.assert_not_called()
        figureBuilder.getRookVectors.assert_not_called()
        figureBuilder.getKnightVectors.assert_not_called()
        figureBuilder.getBishopVectors.assert_not_called()
        figureBuilder.getQueenVectors.assert_called_once_with()
        figureBuilder.getKingVectors.assert_not_called()

    def test_getVectorsByFigureType_KingType_callCorrectMethodAndReturnVectors(self):
        # Arrange
        figureBuilder = self.getBuilderWithFakeGetVectors()

        # Act
        figureBuilder.getVectorsByFigureType(FigureTypeENUM.TYPE_KING)

        # Assert
        figureBuilder.getPawnVectors.assert_not_called()
        figureBuilder.getRookVectors.assert_not_called()
        figureBuilder.getKnightVectors.assert_not_called()
        figureBuilder.getBishopVectors.assert_not_called()
        figureBuilder.getQueenVectors.assert_not_called()
        figureBuilder.getKingVectors.assert_called_once_with()


    def test_getVectorsByFigureType_unknownFigureType_throwRaise(self):
        # Arrange
        figureBuilder = FigureBuilder()
        # Act
        # Assert
        with self.assertRaises(ValueError):
            figureBuilder.getVectorsByFigureType('UnknownFigure')

    def test_build_callGetVectorsByFigureTypeAndReturnFigure(self):
        # Arrange
        figureBuilder = FigureBuilder()

        figureBuilder.getVectorsByFigureType = MagicMock(return_value = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ])

        expected = [
            MoveVector(-1, 0, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, 1, 10, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

        # Act
        actual = figureBuilder.build('SomeFigureType', 'SomeColor', 10)

        # Assert
        self.assertEqual('SomeColor', actual.color)
        self.assertEqual('SomeFigureType', actual.figureType)
        figureBuilder.getVectorsByFigureType.assert_called_once_with('SomeFigureType', 10)

if __name__ == '__main__':
    unittest.main()
