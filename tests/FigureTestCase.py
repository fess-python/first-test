import unittest
from unittest.mock import MagicMock
from unittest.mock import call
from beeprint import pp

import os
import sys

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../'))
sys.path.append(BASE_PATH)
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../BoardGame/'))
sys.path.append(BASE_PATH)

from BoardGame.Figure import *
from BoardGame.Board import Board

class FigureTestCase(unittest.TestCase):
    def __createLocation(self, x, y):
        return Location(x, y)
    def __makeFakeFigure(self, availableLocation):
        class FakeFigure(Figure):
            def getAvailableLocations(self):
                return availableLocation
        return FakeFigure(Color.black)

    def __makeFakeFigureForCanMoveTest(self, setedFigure):
        class FakeFigure(Figure):
            def getAvailableLocations(self):
                return []
        board = Board('abcdefgh', '12345678')
        board.getFigureByLocation = MagicMock(return_value = setedFigure)

        figure = FakeFigure(Color.white)
        figure.setBoard(board)

        return figure

    def __makeFakeFigureForGetAvailableLocationsByStepTest(self, newLocations, canMove):
        class FakeFigure(Figure):
            def getAvailableLocations(self):
                return []
        def getNewLocationSideEffect(self, location = None, diffX = None, diffY = None):
            if not len(newLocations):
                return None
            return newLocations.pop(0)
        def canMoveSideEffect(self, location = None):
            if not len(canMove):
                return None
            return canMove.pop(0)

        board = Board('abcdefgh', '12345678')
        board.getNewLocation = MagicMock(side_effect = getNewLocationSideEffect)

        figure = FakeFigure(Color.white)
        figure.setBoard(board)
        figure.canMove = MagicMock(side_effect = canMoveSideEffect)

        return figure

    def test_move_locationInAvailablilocations(self):
        # Arrange
        locations = [
            self.__createLocation(1, 1),
            self.__createLocation(1, 2),
            self.__createLocation(2, 1)
        ]
        fakeFigure = self.__makeFakeFigure(locations)

        expected = self.__createLocation(1, 2)

        # Act
        fakeFigure.move(expected)
        actual = fakeFigure.getLocation()

        # Assert
        self.assertEqual(expected, actual)

    def test_canMove_locationNotSeted_returnFalse(self):
        # Arrange
        figure = self.__makeFakeFigureForCanMoveTest(None)

        # Act
        actual = figure.canMove(None)

        # Assert
        self.assertFalse(actual)

    def test_canMove_figureNotSeted_returnTrue(self):
        # Arrange
        figure = self.__makeFakeFigureForCanMoveTest(None)
        location = self.__createLocation(1, 1)

        # Act
        actual = figure.canMove(location)

        # Assert
        self.assertTrue(actual)
        figure.board.getFigureByLocation.assert_called_once_with(location)
        

    def test_canMove_setedDifferentColoredFigure_returnTrue(self):
        # Arrange
        setedFigure = self.__makeFakeFigure([])
        setedFigure.color = Color.black

        figure = self.__makeFakeFigureForCanMoveTest(setedFigure)
        location = self.__createLocation(1, 1)

        # Act
        actual = figure.canMove(location)

        # Assert
        self.assertTrue(actual)

    def test_canMove_setedSameColoredFigure_returnFalse(self):
        # Arrange
        setedFigure = self.__makeFakeFigure([])
        setedFigure.color = Color.white

        figure = self.__makeFakeFigureForCanMoveTest(setedFigure)
        location = self.__createLocation(1, 1)

        # Act
        actual = figure.canMove(location)

        # Assert
        self.assertFalse(actual)

    def test_getAvailableLocationsByStep_returnAvailableLocations(self):
        # Arrange
        location = self.__createLocation(1, 2)
        figure = self.__makeFakeFigureForGetAvailableLocationsByStepTest(
            [self.__createLocation(1, 2), self.__createLocation(2, 4), self.__createLocation(3, 1)],
            [True, True, False]
        )
        figure.setLocation(location)

        expected = [self.__createLocation(1, 2), self.__createLocation(2, 4)]
        expectedGetNewLocationCalls = [
            call(location, diffX=1, diffY=1),
            call(location, diffX=2, diffY=2),
            call(location, diffX=3, diffY=3),
        ]
        expectedCanMoveCalls = [
            call(self.__createLocation(1, 2)), 
            call(self.__createLocation(2, 4)),
            call(self.__createLocation(3, 1)),
        ]

        # Act
        actual = figure.getAvailableLocationsByStep(1, 1)

        # Assert
        self.assertEqual(expected, actual)
        self.assertEqual(expectedGetNewLocationCalls, figure.board.getNewLocation.call_args_list)
        self.assertEqual(expectedCanMoveCalls, figure.canMove.call_args_list)
    def test_getAvailableLocationsByStep_zeroDiffXAndDiffY_returnEmptyList(self):
        # Arrange
        figure = self.__makeFakeFigure([])

        # Act
        with self.assertRaises(ValueError):
            figure.getAvailableLocationsByStep(0, 0)

# class PawnTestCase(unittest.TestCase):
#     def __createLocation(self, x, y):
#         return Location(x, y)

#     def __createFigure(self, color, location):
#         figure = Pawn(color)
#         figure.setLocation(location)

#         return figure

#     def __createFigureWithMockBoard(self, color, getNewLocation = [], isEmptyLocation = True, getFigureByLocation = []):
#         def getNewLocationSideEffect(self, location, diffX = 0, diffY = 0):
#             if not len(getNewLocation):
#                 return None
#             return getNewLocation.pop(0)

#         def getFigureByLocationSideEffect(self, location = None):
#             if not len(getFigureByLocation):
#                 return None
#             return getFigureByLocation.pop(0)

#         board = Board('abcdefgh', '12345678')
#         board.isEmptyLocation = MagicMock(return_value = isEmptyLocation)

#         board.getNewLocation = MagicMock(side_effect = getNewLocationSideEffect)
#         board.getFigureByLocation = MagicMock(side_effect = getFigureByLocationSideEffect)


#         figure = Pawn(color)
#         figure.setBoard(board)

#         return figure

#     def test_getForwardMove_canMoveTwoCellsMultiplierMinusOne_returnTwoLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('c', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('c', 5), self.__createLocation('c', 4)]
#         )
#         figure.setLocation(baseLocation)

#         expected = [
#             self.__createLocation('c', 5),
#             self.__createLocation('c', 4)
#         ]

#         expectedCalls = [
#             call(baseLocation, 0, -1),
#             call(baseLocation, 0, -2)
#         ]

#         # Act
#         actual = figure.getForwardMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedCalls, figure.board.getNewLocation.call_args_list)

#     def test_getForwardMove_canMoveTwoCellsMultipleOne_returnTwoLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('c', 7)

#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('c', 8), self.__createLocation('c', 9)]
#         )
#         figure.setLocation(baseLocation)

#         expected = [
#             self.__createLocation('c', 8),
#             self.__createLocation('c', 9)
#         ]

#         expectedCalls = [
#             call(baseLocation, 0, 1),
#             call(baseLocation, 0, 2)
#         ]

#         # Act
#         actual = figure.getForwardMove(1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedCalls, figure.board.getNewLocation.call_args_list)

#     def test_getForwardMove_figureMoved_returnOneLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('c', 6)

#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('c', 5), self.__createLocation('c', 4)]
#         )
#         figure.setLocation(baseLocation)
#         figure.isMoved = True

#         expected = [
#             self.__createLocation('c', 5)
#         ]

#         # Act
#         actual = figure.getForwardMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         figure.board.getNewLocation.assert_called_once_with(baseLocation, 0, -1)

#     def test_getForwardMove_isNotEmptyLocation_returnZeroLocation(self):
#         # Arrange
#         baseLocation = self.__createLocation('c', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('c', 5), self.__createLocation('c', 4)], 
#             isEmptyLocation = False
#         )
#         figure.setLocation(baseLocation)

#         expected = []

#         # Act
#         actual = figure.getForwardMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         figure.board.getNewLocation.assert_called_once_with(baseLocation, 0, -1)

#     def test_getForwardMove_canMoveTwoCellsMultiplierMinusOneAndOnlyOneCorrectLocation_returnOneLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('c', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('c', 5), None]
#         )
#         figure.setLocation(baseLocation)

#         expected = [
#             self.__createLocation('c', 5)
#         ]

#         expectedCalls = [
#             call(baseLocation, 0, -1),
#             call(baseLocation, 0, -2)
#         ]

#         # Act
#         actual = figure.getForwardMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedCalls, figure.board.getNewLocation.call_args_list)

#     def test_getCaptureMove_canCaptureTwoFigures_returnTwoLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('e', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('d', 7), self.__createLocation('f', 7)],
#             getFigureByLocation = [
#                 self.__createFigure(Color.black, self.__createLocation('d', 7)),
#                 self.__createFigure(Color.black, self.__createLocation('f', 7))
#             ]
#         )
#         figure.setLocation(baseLocation)

#         firstCapturedFigure = self.__createLocation('d', 7)
#         secondCapturedFigure = self.__createLocation('f', 7)

#         expected = [
#             firstCapturedFigure,
#             secondCapturedFigure
#         ]

#         expectedGetNewLocationCalls = [
#             call(baseLocation, -1, 1),
#             call(baseLocation, 1, 1)
#         ]

#         expectedGetFigureByLocation = [
#             call(firstCapturedFigure),
#             call(secondCapturedFigure)
#         ]

#         # Act
#         actual = figure.getCaptureMove(1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedGetNewLocationCalls, figure.board.getNewLocation.call_args_list)
#         self.assertEqual(expectedGetFigureByLocation, figure.board.getFigureByLocation.call_args_list)

#     def test_getCaptureMove_canCaptureTwoFiguresWithMinusMultiplier_returnTwoLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('e', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('d', 5), self.__createLocation('f', 5 )],
#             getFigureByLocation = [
#                 self.__createFigure(Color.black, self.__createLocation('d', 5)),
#                 self.__createFigure(Color.black, self.__createLocation('f', 5))
#             ]
#         )
#         figure.setLocation(baseLocation)

#         firstCapturedFigure = self.__createLocation('d', 5)
#         secondCapturedFigure = self.__createLocation('f', 5)

#         expected = [
#             firstCapturedFigure,
#             secondCapturedFigure
#         ]

#         expectedGetNewLocationCalls = [
#             call(baseLocation, -1, -1),
#             call(baseLocation, 1, -1)
#         ]

#         expectedGetFigureByLocation = [
#             call(firstCapturedFigure),
#             call(secondCapturedFigure)
#         ]

#         # Act
#         actual = figure.getCaptureMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedGetNewLocationCalls, figure.board.getNewLocation.call_args_list)
#         self.assertEqual(expectedGetFigureByLocation, figure.board.getFigureByLocation.call_args_list)

#     def test_getCaptureMove_oneEmptyAndOneSameColored_returnZeroLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('e', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('d', 5), self.__createLocation('f', 5 )],
#             getFigureByLocation = [
#                 self.__createFigure(Color.white, self.__createLocation('d', 5)),
#                 None
#             ]
#         )
#         figure.setLocation(baseLocation)

#         firstCapturedFigure = self.__createLocation('d', 5)
#         secondCapturedFigure = self.__createLocation('f', 5)

#         expected = []

#         expectedGetNewLocationCalls = [
#             call(baseLocation, -1, -1),
#             call(baseLocation, 1, -1)
#         ]

#         expectedGetFigureByLocation = [
#             call(firstCapturedFigure),
#             call(secondCapturedFigure)
#         ]

#         # Act
#         actual = figure.getCaptureMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedGetNewLocationCalls, figure.board.getNewLocation.call_args_list)
#         self.assertEqual(expectedGetFigureByLocation, figure.board.getFigureByLocation.call_args_list)

#     def test_getCaptureMove_oneIncorrectLocation_returnOneLocations(self):
#         # Arrange
#         baseLocation = self.__createLocation('e', 6)
#         figure = self.__createFigureWithMockBoard(
#             Color.white, 
#             getNewLocation = [self.__createLocation('d', 5)],
#             getFigureByLocation = [
#                 self.__createFigure(Color.black, self.__createLocation('d', 5)),
#                 self.__createFigure(Color.black, self.__createLocation('f', 5))
#             ]
#         )
#         figure.setLocation(baseLocation)

#         firstCapturedFigure = self.__createLocation('d', 5)
#         secondCapturedFigure = self.__createLocation('f', 5)

#         expected = [
#             firstCapturedFigure
#         ]

#         expectedGetNewLocationCalls = [
#             call(baseLocation, -1, -1),
#             call(baseLocation, 1, -1)
#         ]

#         expectedGetFigureByLocation = [
#             call(firstCapturedFigure)
#         ]

#         # Act
#         actual = figure.getCaptureMove(-1)

#         # Assert
#         self.assertEqual(expected, actual)
#         self.assertEqual(expectedGetNewLocationCalls, figure.board.getNewLocation.call_args_list)
#         self.assertEqual(expectedGetFigureByLocation, figure.board.getFigureByLocation.call_args_list)

#     def test_getAvailableLocations_returnAllLocations(self):
#         # Arrange
#         figure = Pawn('TestColor')
#         figure.getForwardMove = MagicMock(return_value = [
#             self.__createLocation('d', 5),
#             self.__createLocation('d', 3),
#         ])
#         figure.getCaptureMove = MagicMock(return_value = [
#             self.__createLocation('z', 1),
#             self.__createLocation('x', 2),
#         ])
#         figure.getMultiplier = MagicMock(return_value = 1)
        
#         expected = [
#             self.__createLocation('d', 5),
#             self.__createLocation('d', 3),
#             self.__createLocation('z', 1),
#             self.__createLocation('x', 2),
#         ]

#         # Act
#         actual = figure.getAvailableLocations()

#         # Assert
#         self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()