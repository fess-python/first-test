import unittest
from unittest.mock import MagicMock
from beeprint import pp

import os
import sys

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../'))
sys.path.append(BASE_PATH)
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../BoardGame/'))
sys.path.append(BASE_PATH)

from BoardGame.Figure import *
from BoardGame.Board import Board

class BoardTestCase(unittest.TestCase):
    def __createLocation(self, x, y):
        return Location(x, y)

    def __createBoard(self):
        return Board('abcd', '123')

    def test_getNewLocation_xLessThenMinimum_returnNone(self):
        # Arrange 
        board = self.__createBoard()
        location = self.__createLocation('b', '2')

        # Act
        actual = board.getNewLocation(location, -3)

        # Assert
        self.assertIsNone(actual)

    def test_getNewLocation_xMoreThenMaximum_returnNone(self):
        # Arrange 
        board = self.__createBoard()
        location = self.__createLocation('b', '2')

        # Act
        actual = board.getNewLocation(location, 3)

        # Assert
        self.assertIsNone(actual)

    def test_getNewLocation_yLessThenMaximum_returnNone(self):
        # Arrange 
        board = self.__createBoard()
        location = self.__createLocation('b', '2')

        # Act
        actual = board.getNewLocation(location, diffY = -3)

        # Assert
        self.assertIsNone(actual)

    def test_getNewLocation_yMoreThenMaximum_returnNone(self):
        # Arrange 
        board = self.__createBoard()
        location = self.__createLocation('b', '2')

        # Act
        actual = board.getNewLocation(location, diffY = 3)

        # Assert
        self.assertIsNone(actual)

    def test_getNewLocation_allInRange_returnNewLocation(self):
        # Arrange 
        board = self.__createBoard()
        location = self.__createLocation('b', '2')
        expected = self.__createLocation('c', '1')

        # Act
        actual = board.getNewLocation(location, 1, -1)

        # Assert
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
