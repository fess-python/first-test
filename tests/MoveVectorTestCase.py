import unittest
from unittest.mock import MagicMock
from unittest.mock import call
from beeprint import pp

import os
import sys

CUR_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
# BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../'))
# sys.path.append(BASE_PATH)
BASE_PATH = os.path.abspath(os.path.join(CUR_SCRIPT_PATH, '../BoardGame/'))
sys.path.append(BASE_PATH)

from MoveVector import *
from Figure import Figure
from Figure import Color
from Figure import Location
from Board import Board

class MoveVectorTestCase(unittest.TestCase):
    def __createLocation(self, x, y):
        return Location(x, y)
    def __makeFakeFigureForCanMoveTest(self, setedFigure):
        board = Board('abcdefgh', '12345678')
        board.getFigureByLocation = MagicMock(return_value = setedFigure)

        figure = Figure(Color.white)
        figure.setBoard(board)
        
        return figure
    def __makeFakeFigureForGetAvailableMoveTest(self, newLocations):
        def getNewLocationSideEffect(self, location = None, diffX = None, diffY = None):
            if not len(newLocations):
                return None
            return newLocations.pop(0)

        board = Board('abcdefgh', '12345678')
        board.getNewLocation = MagicMock(side_effect = getNewLocationSideEffect)

        figure = Figure(Color.white)
        figure.setBoard(board)
        return figure

    def __makeFakeMoveVectorForGetAvailableMoveTest(self, canMove, maxStep = None):
        def canMoveSideEffect(self, figure = None, location = None):
            if not len(canMove):
                return None
            return canMove.pop(0)

        moveVector = MoveVector(1, 1, maxStep)
        moveVector.canMove = MagicMock(side_effect = canMoveSideEffect)
        return moveVector

    def test_init_locationNotSeted_throwRaise(self):
        # Arrange
        # Act
        # Assert
        with self.assertRaises(ValueError):
            moveVector = MoveVector(0, 0)

    def test_canMove_locationNotSeted_returnFalse(self):
        # Arrange
        moveVector = MoveVector(1, 1)
        figure = Figure(Color.white)

        # Act
        actual = moveVector.canMove(figure, None)

        # Assert
        self.assertFalse(actual)

    def test_canMove_TypeEmpty_returnCorrectResult(self):
        # Arrange
        inputDatas = [
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(Figure(Color.black)),
            	"expected": False
            },
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(None),
            	"expected": True
            },
        ]

        location = Location(1, 1)
        moveVector = MoveVector(1, 1, cellType = CellTypeEnum.TYPE_EMPTY)
        testNumber = 1

        for inputData in inputDatas:
            # Act
            actual = moveVector.canMove(inputData['figure'], location)
            # Assert
            self.assertEqual(actual, inputData['expected'], "Test: " + str(testNumber))
            testNumber += 1

    def test_canMove_TypeEnemyFigure_returnCorrectResult(self):
        # Arrange
        inputDatas = [
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(Figure(Color.black)),
            	"expected": True
            },
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(Figure(Color.white)),
            	"expected": False
            },
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(None),
            	"expected": False
            },
        ]

        location = Location(1, 1)
        moveVector = MoveVector(1, 1, cellType = CellTypeEnum.TYPE_ENEMY_FIGURE)
        testNumber = 1

        for inputData in inputDatas:
            # Act
            actual = moveVector.canMove(inputData['figure'], location)
            # Assert
            self.assertEqual(actual, inputData['expected'], "Test: " + str(testNumber))
            testNumber += 1

    def test_canMove_TypeNotSelfFigure_returnCorrectResult(self):
        # Arrange
        inputDatas = [
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(Figure(Color.black)),
            	"expected": True
            },
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(Figure(Color.white)),
            	"expected": False
            },
            {
            	"figure": self.__makeFakeFigureForCanMoveTest(None),
            	"expected": True
            },
        ]

        location = Location(1, 1)
        moveVector = MoveVector(1, 1, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE)
        testNumber = 1

        for inputData in inputDatas:
            # Act
            actual = moveVector.canMove(inputData['figure'], location)
            # Assert
            self.assertEqual(actual, inputData['expected'], "Test: " + str(testNumber))
            testNumber += 1

    def test_getAvailableMoves_figureNotSpecified_throwRaise(self):
        # Arrange
        moveVector = MoveVector(1, 1)

        # Act
        # Assert
        with self.assertRaises(TypeError):
            moveVector.getAvailableMoves(1)

    def test_getAvailableMoves_notFirstMoveTypeAndFigureNotMoved_returnEmptyMoves(self):
        # Arrange
        moveVector = MoveVector(1, 1, moveType = MoveTypeEnum.TYPE_NOT_FIRST)
        figure = Figure(Color.white)

        # Act
        actual = moveVector.getAvailableMoves(figure);

        # Assert
        self.assertEqual([], actual)

    def test_getAvailableMoves_firstMoveTypeAndFigureMoved_returnEmptyMoves(self):
        # Arrange
        moveVector = MoveVector(1, 1, moveType = MoveTypeEnum.TYPE_FIRST)
        figure = Figure(Color.white)
        figure.isMoved = True

        # Act
        actual = moveVector.getAvailableMoves(figure);

        # Assert
        self.assertEqual([], actual)

    def test_getAvailableMoves_addLocationsWhileCanMove_returnLocations(self):
        # Arrange
        baseLocation = Location('a', 2)
        figure = self.__makeFakeFigureForGetAvailableMoveTest(
            [self.__createLocation('c', 5), self.__createLocation('c', 4)]
        )
        figure.setLocation(baseLocation)

        moveVector = self.__makeFakeMoveVectorForGetAvailableMoveTest([True, True, False])

        expected = [
            self.__createLocation('c', 5),
            self.__createLocation('c', 4)
        ]

        expectedCalls = [
            call(self.__createLocation('a', 2), diffX = 1, diffY = 1),
            call(self.__createLocation('a', 2), diffX = 2, diffY = 2),
            call(self.__createLocation('a', 2), diffX = 3, diffY = 3)
        ]

        # Act
        actual = moveVector.getAvailableMoves(figure)

        # Assert
        self.assertEqual(expected, actual)
        self.assertEqual(expectedCalls, figure.board.getNewLocation.call_args_list)

    def test_getAvailableMoves_addLocationsByMaxStep_returnLocations(self):
        # Arrange
        baseLocation = Location('a', 2)
        figure = self.__makeFakeFigureForGetAvailableMoveTest(
            [self.__createLocation('c', 5), self.__createLocation('c', 4), self.__createLocation('d', 4)]
        )
        figure.setLocation(baseLocation)

        moveVector = self.__makeFakeMoveVectorForGetAvailableMoveTest([True, True, True], 3)

        expected = [
            self.__createLocation('c', 5),
            self.__createLocation('c', 4),
            self.__createLocation('d', 4)
        ]

        expectedCalls = [
            call(self.__createLocation('a', 2), diffX = 1, diffY = 1),
            call(self.__createLocation('a', 2), diffX = 2, diffY = 2),
            call(self.__createLocation('a', 2), diffX = 3, diffY = 3)
        ]

        # Act
        actual = moveVector.getAvailableMoves(figure)

        # Assert
        self.assertEqual(expected, actual)
        self.assertEqual(expectedCalls, figure.board.getNewLocation.call_args_list)


if __name__ == '__main__':
    unittest.main()