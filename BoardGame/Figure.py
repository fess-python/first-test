# from abc import ABCMeta, abstractmethod, ABC

from beeprint import pp

class Figure:
    location = None
    board = None
    color = None
    isMoved = False
    figureType = None
    moveVectors = []

    def __init__(self, color, figureType = None):
        self.color = color
        self.figureType = figureType
        self.moveVectors = []

    def setBoard(self, board):
        self.board = board

    def setLocation(self, location):
        self.location = location

    def getLocation(self):
        return self.location

    def addMoveVector(self, moveVector):
        self.moveVectors.append(moveVector)

    def getMoveVectors(self):
        return self.moveVectors

    def move(self, newLocation): 
        availableLocation = self.getAvailableLocations();

        if not newLocation in availableLocation:
            raise ValueError("Невозможно передвинуть фигуру")

        self.setLocation(newLocation)

        self.isMoved = True

    def getAvailableLocations(self):
        availableLocations = []
        for moveVector in self.moveVectors:
            availableLocations += moveVector.getAvailableMoves(self)

        return availableLocations

class Color:
    white = "white"
    black = "black"

class Location:
    """
    Класс координаты
    """
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __eq__(self, element):
        return self.x == element.x and self.y == element.y