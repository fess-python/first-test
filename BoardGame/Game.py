import Board
from Figure import *
from MoveVector import *
from Chess.FigureBuilder import *

from beeprint import pp

class Chess:
    board = None
    currentMoveColor = None
    def __init__(self):
        self.board = Board.Board('abcdefgh', '12345678')
        self.__setFigure(self.__makeFogureCoordinates('2', '1'), Color.white, 1)
        self.__setFigure(self.__makeFogureCoordinates('7', '8'), Color.black, -1)
        self.currentMoveColor = Color.white

    def getBoard(self):
        return self.board

    def move(self, sourceLocation, destinationLocation):
        figure = self.board.getFigureByLocation(sourceLocation)

        if not figure:
            raise ValueError('По указанным координатам нет фигуры')

        if figure.color != self.currentMoveColor:
            raise ValueError('Выбрана фигура не того цвета')

        figure.move(destinationLocation)

        self.currentMoveColor = Color.black if self.currentMoveColor == Color.white else Color.white

    def __makeFogureCoordinates(self, pawnRowNum, nonPawnRowNum):
        return {
            FigureTypeENUM.TYPE_PAWN: [(x, pawnRowNum) for x in 'abcdefgh'],
            FigureTypeENUM.TYPE_ROOK: [('a', nonPawnRowNum), ('h', nonPawnRowNum)],
            FigureTypeENUM.TYPE_KNIGHT: [('b', nonPawnRowNum), ('g', nonPawnRowNum)],
            FigureTypeENUM.TYPE_BISHOP: [('c', nonPawnRowNum), ('f', nonPawnRowNum)],
            FigureTypeENUM.TYPE_QUEEN: [('d', nonPawnRowNum)],
            FigureTypeENUM.TYPE_KING: [('e', nonPawnRowNum)]
        }
    
    """Выставляет фигуры на поле"""
    def __setFigure(self, figureCoordinates, color, diffY = None):
        figureBuilder = self.__getFigureBuilder();
        for figureType in figureCoordinates:
            for coordinate in figureCoordinates[figureType]:
                figure = figureBuilder.build(figureType, color, diffY)
                figure.setLocation(Location(coordinate[0], coordinate[1]))
                self.board.addFigure(figure)

    def __getFigureBuilder(self):
        return FigureBuilder()