from Figure import Figure
from Figure import Location
from Figure import Color

class Board:
    """
    Класс шахматной доски
    """
    # Список фигур
    figures = []

    x = None
    y = None

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def addFigure(self, figure):
        if not isinstance(figure, Figure):
            raise TypeError("figure must be instance of Figure.Figure")
        figure.setBoard(self)
        self.figures.append(figure)

    def getFigureByLocation(self, location):
        for figure in self.figures:
            if figure.location == location:
                return figure
        return None

    def isEmptyLocation(self, location):
        figure = self.getFigureByLocation(location)
        return figure == None

    def isCorrectLocation(self, location):
        return location.x in self.x and location.y in self.y

    def getNewLocation(self, currentLocation, diffX = 0, diffY = 0):
        maxXCount = len(self.x)
        maxYCount = len(self.y)

        currentXPosition = self.x.find(str(currentLocation.x))
        currentYPosition = self.y.find(str(currentLocation.y))

        newXPosition = currentXPosition + diffX
        newYPosition = currentYPosition + diffY

        if newXPosition < 0 or newXPosition >= maxXCount:
            return None

        if newYPosition < 0 or newYPosition >= maxYCount:
            return None

        return Location(self.x[newXPosition], self.y[newYPosition])

    def move(self, sourceLocation, destinationLocation):
        if not self.isCorrectLocation(sourceLocation):
            raise ValueError("Исходная клетка вне поля")

        if not self.isCorrectLocation(destinationLocation):
            raise ValueError("Результирующая клетка вне поля")

        sourceFigure = self.getFigureByLocation(sourceLocation)
        destinationFigure = self.getFigureByLocation(destinationLocation)

        if not sourceFigure:
            raise ValueError("На указанном поле нет фигуры")

        if destinationFigure and destinationFigure.color == sourceFigure.color:
            raise ValueError("Невозможно рубить фигуру того же цвета")

        sourceFigure.move(destinationLocation)