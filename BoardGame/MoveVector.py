from Figure import Figure

from beeprint import pp

class MoveVector:
    stepX = None
    stepY = None
    maxStep = None
    cellType = None
    moveType = None

    def __init__(self, stepX, stepY, maxStep = None, cellType = "not_self_figure", moveType = "any"):
        if not stepX and not stepY:
            raise ValueError('Необходимо указать смещение по X и/или по Y')
        self.stepX = stepX
        self.stepY = stepY
        self.maxStep = maxStep
        self.cellType = cellType
        self.moveType = moveType

    def getAvailableMoves(self, figure):
        if not isinstance(figure, Figure):
            raise TypeError("Вектор движения может работать только с фигурой")

        locations = []

        if self.moveType == MoveTypeEnum.TYPE_FIRST and figure.isMoved:
            return locations
        if self.moveType == MoveTypeEnum.TYPE_NOT_FIRST and not figure.isMoved:
            return locations

        diffX = self.stepX
        diffY = self.stepY

        currentStepCount = 1

        while True:
            newLocation = figure.board.getNewLocation(figure.location, diffX = diffX, diffY = diffY)
            # Если невозможно двигаться на указанную клетку - движение останавливается
            if not self.canMove(figure, newLocation):
                break

            locations.append(newLocation)

            diffX += self.stepX
            diffY += self.stepY

            currentStepCount += 1

            # Если установлено максимальное количество шагов, и мы уже сделали столько шагов - зватит двигаться
            if self.maxStep and currentStepCount > self.maxStep:
                break

        return locations

    def canMove(self, figure, location):
        if not location:
            return False
        setedFigure = figure.board.getFigureByLocation(location)

        return {
            self.cellType == CellTypeEnum.TYPE_EMPTY: setedFigure == None,
            self.cellType == CellTypeEnum.TYPE_ENEMY_FIGURE: setedFigure != None and setedFigure.color != figure.color,
            self.cellType == CellTypeEnum.TYPE_NOT_SELF_FIGURE: setedFigure == None or setedFigure.color != figure.color,
        }.get(1, False)
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False

        return (
            self.stepX == other.stepX and
            self.stepY == other.stepY and
            self.maxStep == other.maxStep and
            self.cellType == other.cellType and
            self.moveType == other.moveType
        )

class CellTypeEnum:
    TYPE_EMPTY = "empty"
    TYPE_ENEMY_FIGURE = "enemy_figure"
    TYPE_NOT_SELF_FIGURE = "not_self_figure"

class MoveTypeEnum:
    TYPE_FIRST = "fitst"
    TYPE_NOT_FIRST = "not_first"
    TYPE_ANY = "any"