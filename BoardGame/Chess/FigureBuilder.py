from MoveVector import *

class FigureBuilder:
    def build(self, figureType, color, diffY = None):
        vectors = self.getVectorsByFigureType(figureType, diffY);

        figure = Figure(color, figureType)

        for vector in vectors:
            figure.addMoveVector(vector)

        return figure

    def getVectorsByFigureType(self, figureType, diffY=None):
        if figureType == FigureTypeENUM.TYPE_PAWN:
            return self.getPawnVectors(diffY)
        elif figureType == FigureTypeENUM.TYPE_ROOK:
            return self.getRookVectors()
        elif figureType == FigureTypeENUM.TYPE_KNIGHT:
            return self.getKnightVectors()
        elif figureType == FigureTypeENUM.TYPE_BISHOP:
            return self.getBishopVectors()
        elif figureType == FigureTypeENUM.TYPE_QUEEN:
            return self.getQueenVectors()
        elif figureType == FigureTypeENUM.TYPE_KING:
            return self.getKingVectors()

        raise ValueError("Неизвестная фигура")

    """Создает векторы движения пешки"""
    def getPawnVectors(self, diffY):
        return [
            MoveVector(0, diffY, maxStep = 2, cellType = CellTypeEnum.TYPE_EMPTY, moveType = MoveTypeEnum.TYPE_FIRST),
            MoveVector(0, diffY, maxStep = 1, cellType = CellTypeEnum.TYPE_EMPTY, moveType = MoveTypeEnum.TYPE_NOT_FIRST),
            MoveVector(1, diffY, maxStep = 1, cellType = CellTypeEnum.TYPE_ENEMY_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, diffY, maxStep = 1, cellType = CellTypeEnum.TYPE_ENEMY_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

    """Создает векторы движения ладьи"""
    def getRookVectors(self):
        return self.getHorizontalMoveVectors(None) + self.getVerticalMoveVectors(None)

    """Создает векторы движения коня"""
    def getKnightVectors(self):
        return [
            MoveVector(-1, 2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, 2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, -2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, -2, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-2, -1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-2, 1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(2, -1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(2, 1, maxStep = None, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

    """Создает векторы движения офицера"""
    def getBishopVectors(self):
        return self.getDiagonalMoveVectors(None)
        

    """Создает векторы движения ферзя"""
    def getQueenVectors(self):
        return self.getDiagonalMoveVectors(None) + self.getHorizontalMoveVectors(None) + self.getVerticalMoveVectors(None)

    """Создает векторы движения короля"""
    def getKingVectors(self):
        return self.getDiagonalMoveVectors(1) + self.getHorizontalMoveVectors(1) + self.getVerticalMoveVectors(1)

    def getDiagonalMoveVectors(self, maxStep):
        return [
            MoveVector(1, 1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(1, -1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, -1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

    def getVerticalMoveVectors(self, maxStep):
        return [
            MoveVector(0, 1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(0, -1, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

    def getHorizontalMoveVectors(self, maxStep):
        return [
            MoveVector(1, 0, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
            MoveVector(-1, 0, maxStep, cellType = CellTypeEnum.TYPE_NOT_SELF_FIGURE, moveType = MoveTypeEnum.TYPE_ANY),
        ]

class FigureTypeENUM:
    # Пешка
    TYPE_PAWN = "pawn"
    # Ладья
    TYPE_ROOK = "rook"
    # Конь
    TYPE_KNIGHT = "knight"
    # Слон
    TYPE_BISHOP = "bishop"
    # Ферзь
    TYPE_QUEEN = "queen"
    # Король
    TYPE_KING = "king"
